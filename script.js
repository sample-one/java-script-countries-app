 
const API_URL = "https://restcountries.com/v3.1/all"

const displayCountries = (countries) =>{
  const container = document.getElementById("country-container");
 
 countries.forEach(country =>{
   const countryArticle = document.createElement('article');
   countryArticle.className = 'country-card';
 
   const flag = document.createElement('img');
   flag.className = 'country-flag';
   flag.src = country.flags.svg;
   
   const countryName = document.createElement('h3')
   countryName.textContent = country.name.common;
 
 
   countryArticle.appendChild(flag);
   countryArticle.appendChild(countryName);
   container.appendChild(countryArticle);
 
 
 })
 
 
 }


 const countryData = async function getData(){

 try{
  const response = await fetch(API_URL);
 if (!response.ok){
  console.log("fetching error")
  return;

 }

 let countriesInfo =  await response.json();
 console.log("country data are",countriesInfo);

 displayCountries(countriesInfo);
 }  
 

 catch(error){
  console.log("error",error);
 }



}

countryData(); 


 